-- Michael Griffin
-- Latest Database and Tables 12/12/2019


-- Create the TEST Database first
CREATE DATABASE `test` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

-- Switch to TEST database before trying to create tables.
use test;

-- Next Create All Tables needed.
CREATE TABLE `user_type_lk` (
  `index` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`index`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `registration` (
  `index` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `date_of_birth` date NOT NULL,
  `gender` varchar(45) NOT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `vendor_license` varchar(45) DEFAULT NULL,
  `user_type` int(11) NOT NULL,
  PRIMARY KEY (`index`),
  UNIQUE KEY `index_UNIQUE` (`index`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `user_type_fk_idx` (`user_type`),
  CONSTRAINT `user_type_fk` FOREIGN KEY (`user_type`) REFERENCES `user_type_lk` (`index`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;


CREATE TABLE `authentication` (
  `index` int(11) NOT NULL AUTO_INCREMENT,
  `login_id` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `password` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `registration` int(11) DEFAULT NULL,
  PRIMARY KEY (`index`),
  UNIQUE KEY `login_id_UNIQUE` (`login_id`),
  UNIQUE KEY `registration_UNIQUE` (`registration`),
  CONSTRAINT `registration` FOREIGN KEY (`registration`) REFERENCES `registration` (`index`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
SELECT * FROM test.registration;


CREATE TABLE `vendor` (
  `index` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `route` int(11) DEFAULT NULL,
  `contact` int(11) DEFAULT NULL,
  PRIMARY KEY (`index`),
  KEY `contact_fk_idx` (`contact`),
  CONSTRAINT `contact_fk` FOREIGN KEY (`contact`) REFERENCES `registration` (`index`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `menu` (
  `index` int(11) NOT NULL AUTO_INCREMENT,
  `vendor` int(11) DEFAULT NULL,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`index`),
  KEY `vendor_fk_idx` (`vendor`),
  CONSTRAINT `vendor_fk` FOREIGN KEY (`vendor`) REFERENCES `vendor` (`index`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;


CREATE TABLE `item` (
  `index` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `menu` int(11) DEFAULT NULL,
  PRIMARY KEY (`index`),
  KEY `FKodihaxbya5u9ymr7sv79qt8dw` (`menu`),
  CONSTRAINT `FKodihaxbya5u9ymr7sv79qt8dw` FOREIGN KEY (`menu`) REFERENCES `menu` (`index`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;


CREATE TABLE `location` (
  `index` int(11) NOT NULL AUTO_INCREMENT,
  `location` int(11) DEFAULT NULL,
  `vendor` int(11) DEFAULT NULL,
  PRIMARY KEY (`index`),
  UNIQUE KEY `index_UNIQUE` (`index`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Insert Data into the Lookup table for User Types!
insert into user_type_lk (`index`,`name`) values (0,'User');
insert into user_type_lk (`index`,`name`) values (1,'Vendor');
insert into user_type_lk (`index`,`name`) values (2,'Administrator');

