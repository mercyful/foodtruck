Food Truck Finder - JSF Web Application on a MySQL Database
=============================

Michael Griffin 12/12/2019
mrmisticismo@hotmail.com

Free to all for learning - No licence and not responsible for any damage from misuse of this application or it's code.
Enjoy!


Developer Environment Setup
---------------------------
The project has been created in Netbean version 8.02
This is a Web Java EE project which takes advantage of Maven for java dependencies.

This project makes use of Tomcat's JNDI environment for handling Database connection sources.

**[Project Folder]/tomcat**
```
There is a folder in the root project called **Tomcat** you will need to copy the **server.xml** file to your the root **Tomcat** folder.
along with the lib folder which has the latest MySQL database connector.  This file also needs to be copied to the **Tomcat/lib** folder.

At this point Tomcat will handle all database connection and not SQL drivers needs to imported directly into the project.
Login credentials and passwords are then stored in Tomcat's server.xml file.   This can able be updated later on with system properties that
can be passed on command line during startup for Docker containers.
```

**[Project Folder]/docs**
```
This has a SQL script and all statements needed to created the Database, Tables, and Populate an initial Lookup table used by the application.
```

Project Status
---------------------------
```
Login              [Completed]
Registration       [Completed]
Vendor Menu Editor [Completed]
Dashboards         [Started]
Breadcrumbs        [Started]
Vendor Admin       [Started]
Session Security   [Started]
```


Database Notes
---------------------------
The default database name is `test` on MySQL
As mentioned all of the needed SQL to create the tables are in the root docs folder.

Web.XML Notes
---------------------------
```
This is the heart of a site configuration, there is also a nice developer tool called WELD.  It's disabled by default. However, you
can enable this in the web.xml file - once reach the start page you will see weld access to your stats.  Check it out.
```


Final Notes
---------------------------

At this point you can register as either a new **User** or **Vendor**.
```
- Users can login and get to the User Dashboard, but there are no other functions.
- Vendors can login to the Vendor Dashboard and do full CRUD operations creating Menu Items with a Name and Price for each Item.
```

The future would be to expand the Dashboards for searching Vendors, Making Menu Sections ie.. Entree's, App's, Deserts
Adding User Dashboard functions for searching Vendor and going through their Menus and Locations etc.

This is a very simple application to demonstrate SJF with Primefaces.  Some useful ORM functionally with the EntityManager vs. using the SessionFactory.
There is not allot of session security right now. So this could be added and improved a bit following OWASP.  There are is some basic session
security, when you login it will use session attributes to verify user and page access.


