/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.griffin.foodtruck.common;

import javax.servlet.ServletContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Thread Safe Context
 * @author griffin
 */
public class ServletContextMgr {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(ServletContextMgr.class.getName());
    
    private volatile static ServletContextMgr manager;	
    private ServletContext context = null;

    private ServletContextMgr() {}
        
    // Use synchronization for thread safe singleton creation.
    public static ServletContextMgr getInstance() {
        if (manager == null) {
            synchronized(ServletContextMgr.class) {
                try {
                    if (manager == null)
                        manager = new ServletContextMgr(); 
                } catch(Exception ex) {
                    LOGGER.error("Initial ServletContextMgr creation failed. ", ex);
                    throw new ExceptionInInitializerError(ex);
                }
            }
        }
        return manager;
    }

    public ServletContext getContext() {
        return context;
    }

    public void setContext(ServletContext context) {
        this.context = context;
    }
}
