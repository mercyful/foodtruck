/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.griffin.foodtruck.constants;

/**
 *
 * @author Blue
 */
public class ProjectConstants {
    public static final String APP_ID = "foodTruck";
    
    public static final boolean LOGIN_VALIDATION = true;
    public static final boolean REGISTER_VALIDATION = false;
    
    public static final String TARGET_LOGIN = "/login.xhtml";
    public static final String TARGET_LOGIN_REDIRECT = "/login.xhtml?faces-redirect=true";
    public static final String TARGET_REGISTER = "/register.xhtml";
    
    public static final String TARGET_REGISTER_REDIRECT = "/register.xhtml?faces-redirect=true";
  
    public static final String TARGET_MAIN = "/main.xhtml";
    public static final String TARGET_MAIN_REDIRECT = "/main.xhtml?faces-redirect=true";
    
    public static final String SESSION_AUTHORIZED = "authorized";
    public static final String SESSION_STATE_REGISTER = "register";
    public static final String SESSION_STATE_AUTHORIZED = "true";
    
    // TODO This could be replaced with a class instead of individual fields.
    public static final String USER_NAME = "user_name";
    public static final String USER_PASS = "user_pass";
    public static final String USER_TYPE = "user_type";
    
    public static final String TARGET_VENDOR = "/adm_vendor.xhtml";
    public static final String TARGET_VENDOR_REDIRECT = "/adm_vendor.xhtml?faces-redirect=true";
}
