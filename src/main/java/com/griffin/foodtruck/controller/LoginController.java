/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.griffin.foodtruck.controller;

import com.griffin.foodtruck.constants.ProjectConstants;
import com.griffin.foodtruck.entity.Registration;
import com.griffin.foodtruck.manager.AuthorizationManager;
import com.griffin.foodtruck.manager.MessageManager;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.primefaces.component.tabview.Tab;
import org.primefaces.component.tabview.TabView;
import org.primefaces.event.TabChangeEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Authentication
 *
 * @author Blue
 */
@Named
@ViewScoped
public class LoginController implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoginController.class.getName());
    private static final long serialVersionUID = 1L;
    
    private String loginId = "";
    private String password = "";
    private String registerId = "";    
    private String registerPass = "";
    private int tabview;
    

    protected AuthorizationManager authManager;

    /**
     * Dependency Injected Constructor
     *
     * @param authManager
     */
    @Inject
    public LoginController(AuthorizationManager authManager) {
        this.authManager = authManager;
    }

    @PostConstruct
    public void clearMessages() {
        MessageManager.clearAllMessages();
    }

    /**
     * Validate User Name and Password
     *
     * @param loginOrRegister
     * @return
     */
    public boolean validateUserNamePassword(boolean loginOrRegister) {
        
        if (loginOrRegister) {
            if (loginId == null || loginId.isEmpty()) {
                MessageManager.addErrorMessage("Username is required.");
                LOGGER.error("Username is required.");
                return false;
            }

            if (password == null || password.isEmpty()) {
                MessageManager.addErrorMessage("Password is required.");
                LOGGER.error("password is required.");
                return false;
            }
        }
        else {
            if (registerId == null || registerId.isEmpty()) {
                MessageManager.addErrorMessage("Username is required.");
                LOGGER.error("Username is required.");
                return false;
            }

            if (registerPass == null || registerPass.isEmpty()) {
                MessageManager.addErrorMessage("Password is required.");
                LOGGER.error("password is required.");
                return false;
            }
        }

        LOGGER.info(loginId, "Validation of username and password successfull.");
        return true;
    }

    /**
     * Clear Error / Info Messages on Tag Change Between Sign-up and Register
     * @param event
     */
    public void onTabChange(TabChangeEvent event) {
        MessageManager.clearAllMessages();       
        Tab activeTab = event.getTab();
        tabview = ((TabView)event.getSource()).getChildren().indexOf(activeTab);
    }

    /**
     * Tests the Main.xhtml page if the session is valid, if not won't display it!
     * @return 
     */
    public boolean isValidSession() {        
        FacesContext ftx = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) ftx.getExternalContext().getRequest();
        HttpSession session = request.getSession(false);
        
        if (ProjectConstants.SESSION_STATE_AUTHORIZED.equals((String)session.getAttribute(ProjectConstants.SESSION_AUTHORIZED))){
            return true;
        }
        
        return false;
    }    
    
    /**
     * Tests the Main.xhtml page if the session is valid, if not won't display it!
     * @return 
     */
    public boolean isValidRegistration() {        
        FacesContext ftx = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) ftx.getExternalContext().getRequest();
        HttpSession session = request.getSession(false);
        
        if (ProjectConstants.SESSION_STATE_REGISTER.equals((String)session.getAttribute(ProjectConstants.SESSION_AUTHORIZED))){
            return true;
        }
        
        return false;
    }
    
    /**
     * Performs all possible logout and session cleanup tasks
     * @return 
     */
    public static String logout() {
        
        String target = ProjectConstants.TARGET_LOGIN_REDIRECT;
        
        FacesContext ftx = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) ftx.getExternalContext().getRequest();
        HttpServletResponse response = (HttpServletResponse) ftx.getExternalContext().getResponse();
        HttpSession session = request.getSession(false);
        MessageManager.clearAllMessages();
        
        MessageManager.addInfoMessage("Session is successfully Logged out.");
        
        if (session != null) {            
            session.removeAttribute(ProjectConstants.SESSION_AUTHORIZED);
            session.removeAttribute(ProjectConstants.USER_NAME);
            session.invalidate();
        }
        try {
            request.logout();            
            LOGGER.info("Session is successfully logged out.");
        } catch (ServletException e) {
            LOGGER.error("Error shutting logging out of session: ", e);
        }
        
        return target;
    }

    /**
     * Gets User Name from the logged in Session.
     *
     * @return
     */
    public String getUserName() {
        FacesContext ftx = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) ftx.getExternalContext().getRequest();
        HttpSession session = request.getSession(false);
        String username = (String) session.getAttribute(ProjectConstants.USER_NAME);
        return username;
    }

    /**
     * Gets User Password from the logged in Session for Registration
     *
     * @return
     */
    public String getRegistrationPassword() {
        FacesContext ftx = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) ftx.getExternalContext().getRequest();
        HttpSession session = request.getSession(false);
        String userPass = (String) session.getAttribute(ProjectConstants.USER_PASS);
        return userPass;
    }
    
    /**
     * Gets User Password from the logged in Session for Registration
     *
     * @return
     */
    public String getUserType() {
        FacesContext ftx = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) ftx.getExternalContext().getRequest();
        HttpSession session = request.getSession(false);
        String userType = (String) session.getAttribute(ProjectConstants.USER_TYPE);
        return userType;
    }

     /**
     * Main Login Click after Validation is Successful
     *
     * @return
     * @throws java.lang.Exception
     */
    public String register() throws Exception {

        // Valid fields passing through on Submit!
        LOGGER.info("register_id: " + registerId);
        LOGGER.info("register_pass: " + registerPass);

        String target = ProjectConstants.TARGET_LOGIN_REDIRECT;
        
        // Set default target.
        MessageManager.clearAllMessages();
        FacesContext ftx = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) ftx.getExternalContext().getRequest();

        // Get the session without creating a new session.
        HttpSession session = request.getSession(false);

        // Validate fields are not empty
        boolean validation = validateUserNamePassword(ProjectConstants.REGISTER_VALIDATION);
        if (!validation) {
            // Avoids swtiching back to Signup TAB.
            return "";
        }
        
        // Check Login and Password exists in database
        try {
            boolean authExists = authManager.doesAuthorizationExist(registerId);
            if (!authExists) {
                // Validated, move to hext target
                target = ProjectConstants.TARGET_REGISTER_REDIRECT;
                session.setAttribute(ProjectConstants.SESSION_AUTHORIZED, ProjectConstants.SESSION_STATE_REGISTER);
                session.setAttribute(ProjectConstants.USER_NAME, registerId);
                session.setAttribute(ProjectConstants.USER_PASS, registerPass);

            } else {
                MessageManager.addErrorMessage("Invalid Registration - Username Already Exists, Please try again.");
            }
        } catch (Exception ex) {
            MessageManager.addErrorMessage("System Exception: " + ex.getMessage());
        }
        
        // clear out and don't keep in memory.
        registerId = "";
        registerPass = "";

        return target;
    }
    
    /**
     * Main Login Click after Validation is Successful
     *
     * @return
     * @throws java.lang.Exception
     */
    public String login() throws Exception {

        // Valid fields passing through on Submit!
        LOGGER.info("login_id: " + loginId);
        LOGGER.info("passsword: " + password);

        String target = ProjectConstants.TARGET_LOGIN_REDIRECT;

        // Set default target.
        MessageManager.clearAllMessages();
        FacesContext ftx = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) ftx.getExternalContext().getRequest();

        // Get the session without creating a new session.
        HttpSession session = request.getSession(false);

        // Validate fields are not empty
        boolean validation = validateUserNamePassword(ProjectConstants.LOGIN_VALIDATION);
        if (!validation) {
            return "";
        }

        // Check Login and Password exists in database
        try {
            boolean authenticated = authManager.checkAuthorization(loginId, password);
            if (authenticated) {
                target = ProjectConstants.TARGET_MAIN;
                session.setAttribute(ProjectConstants.SESSION_AUTHORIZED, ProjectConstants.SESSION_STATE_AUTHORIZED);
                session.setAttribute(ProjectConstants.USER_NAME, loginId);
                
                // Store the UserType, or later on the User Record in the Session for pulling Information in other Controllers.
                Registration userRecord = authManager.retrieveRegistrationByLoginId(loginId);
                session.setAttribute(ProjectConstants.USER_TYPE, userRecord.getUserType().getName());

            } else {
                MessageManager.addErrorMessage("Invalid Login Attempt, please check loginId and password.");
            }
        } catch (Exception ex) {
            MessageManager.addErrorMessage("System Exception: " + ex.getMessage());
        }
        
        // clear out and don't keep in memory.
        loginId = "";
        password = "";
        
        return target;        
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRegisterId() {
        return registerId;
    }

    public void setRegisterId(String registerId) {
        this.registerId = registerId;
    }

    public String getRegisterPass() {
        return registerPass;
    }

    public void setRegisterPass(String registerPass) {
        this.registerPass = registerPass;
    }

    public int getTabview() {
        return tabview;
    }

    public void setTabview(int tabview) {
        this.tabview = tabview;
    }

    public AuthorizationManager getAuthManager() {
        return authManager;
    }

    public void setAuthManager(AuthorizationManager authManager) {
        this.authManager = authManager;
    }

    

}
