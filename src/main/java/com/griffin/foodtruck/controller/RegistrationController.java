/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.griffin.foodtruck.controller;

import com.griffin.foodtruck.constants.ProjectConstants;
import com.griffin.foodtruck.entity.Authentication;
import com.griffin.foodtruck.entity.Menu;
import com.griffin.foodtruck.entity.Registration;
import com.griffin.foodtruck.entity.UserTypeLk;
import com.griffin.foodtruck.entity.Vendor;
import com.griffin.foodtruck.manager.AuthorizationManager;
import com.griffin.foodtruck.manager.MenuManager;
import com.griffin.foodtruck.manager.MessageManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.primefaces.event.FlowEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Authentication
 *
 * @author Blue
 */
@Named
@ViewScoped
public class RegistrationController implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(RegistrationController.class.getName());
    private static final long serialVersionUID = 1L;
    private static final String VENDOR_USER_TYPE = "Vendor";

    // Registration Entity
    private Registration register = new Registration();    
    private List<SelectItem> genderTypes = new ArrayList<>();
    private List<SelectItem> userTypes = new ArrayList<>();
    private String selectedUserType;
    private Vendor vendor = new Vendor();

    protected AuthorizationManager authManager;
    protected MenuManager menuManager;

    /**
     * Dependency Injected Constructor
     *
     * @param authManager
     * @param menuManager
     */
    @Inject
    public RegistrationController(AuthorizationManager authManager, MenuManager menuManager) {
        this.authManager = authManager;
        this.menuManager = menuManager;
    }

    @PostConstruct
    public void clearMessages() {
        MessageManager.clearAllMessages();
        init();
    }

    public void init() {
        try {
            // Setup Gender Types
            SelectItem genderItem = new SelectItem();
            genderItem.setLabel("-- Gender --");
            genderItem.setNoSelectionOption(true);
            genderTypes.add(genderItem);
            genderTypes.add(new SelectItem("Male", "Male"));
            genderTypes.add(new SelectItem("Female", "Female"));

            // Retrieve list of available user Types.
            List<UserTypeLk> types = authManager.getUserTypes();

            // List User Types.. testing.
            SelectItem item = new SelectItem();
            item.setLabel("-- User Type --");
            item.setNoSelectionOption(true);
            userTypes.add(item);

            types.forEach((t) -> {
                // All all items ecept for administrator.
                if (!t.getName().equalsIgnoreCase("Administrator")) {
                    userTypes.add(new SelectItem(t.getName(), t.getName()));
                    LOGGER.info("User Type: " + t.getName() + ' ' + t.getIndex());
                }                
            });
        } catch (Exception e) {
            LOGGER.error("Exception pulling in userTypes ", e);
            MessageManager.addErrorMessage("Error retrieving User Types");
        }
    }

    /**
     * Flow Event for PrimeFaces wizard 
     * @param event
     * @return 
     */
    public String onFlowProcess(FlowEvent event) {
        return event.getNewStep();
    }

    /**
     * Validate User Name and Password
     *
     * @return
     */
    public boolean validateRegistrationForm() {
        if (register.getFirstName() == null || register.getFirstName().isEmpty()) {
            MessageManager.addErrorMessage("First Name is required.");
            LOGGER.error("First Name is required.");
            return false;
        }

        if (register.getLastName() == null || register.getLastName().isEmpty()) {
            MessageManager.addErrorMessage("Last Name is required.");
            LOGGER.error("Last Name is required.");
            return false;
        }

        if (register.getEmail() == null || register.getEmail().isEmpty()) {
            MessageManager.addErrorMessage("Email Address is required.");
            LOGGER.error("Email Address is required.");
            return false;
        }

        if (register.getDateOfBirth() == null) {
            MessageManager.addErrorMessage("Date Of Birth is required.");
            LOGGER.error("Date Of Birth is required.");
            return false;
        }

        if (register.getGender() == null || register.getGender().isEmpty()) {
            MessageManager.addErrorMessage("Gender is required.");
            LOGGER.error("Gender is required.");
            return false;
        }

        if (register.getUserType() == null || register.getUserType().getName() == null || register.getUserType().getName().isEmpty()) {
            MessageManager.addErrorMessage("User Type is required.");
            LOGGER.error("User Type required.");
            return false;
        }

        return true;
    }

    /**
     * Gets User Name from the logged in Session.
     *
     * @return
     */
    public String getUserName() {
        FacesContext ftx = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) ftx.getExternalContext().getRequest();
        HttpSession session = request.getSession(false);
        String username = (String) session.getAttribute(ProjectConstants.USER_NAME);
        return username;
    }

    /**
     * Gets User Password from the logged in Session for Registration
     *
     * @return
     */
    public String getRegistrationPassword() {
        FacesContext ftx = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) ftx.getExternalContext().getRequest();
        HttpSession session = request.getSession(false);
        String userPass = (String) session.getAttribute(ProjectConstants.USER_PASS);
        return userPass;
    }

    /**
     * Main Login Click after Validation is Successful
     *
     * @return
     * @throws java.lang.Exception
     */
    public String saveRegistration() throws Exception {

        String target = ProjectConstants.TARGET_REGISTER;

        LOGGER.info("Selcted UserType : " + selectedUserType);

        // Retrieve list of available user Types.
        // Then assign per the selection Made through the Page.
        List<UserTypeLk> types = authManager.getUserTypes();
        types.forEach((UserTypeLk t) -> {
            if (t.getName().equals(selectedUserType)) {
                register.setUserType(t);
                LOGGER.info("User Type Set as : " + t.getName());
            }
        });

        boolean isValid = validateRegistrationForm();
        if (isValid) {

            // Save new record to the database.
            Authentication authRecord = new Authentication();
            authRecord.setLoginId(getUserName());
            authRecord.setPassword(getRegistrationPassword());
            authRecord.setRegistration(register);

            try {
                // Created Authentication and Registration Records
                authManager.saveRegistration(authRecord);

                if (VENDOR_USER_TYPE.equals(selectedUserType)) {
                    vendor.setContact(register);                        
                    LOGGER.info("*** SAVING VENDOR *** as : " + vendor.getName());
                    authManager.saveVendor(vendor);

                    Menu mnu = new Menu();
                    mnu.setName("Main Menu");                
                    mnu.setVendor(vendor);
                    LOGGER.info("*** SAVING VENDOR MENU *** as : " + vendor.getName());
                    menuManager.saveMenu(mnu);
                }
            }
            catch (Exception e) {
                MessageManager.addErrorMessage("Registration - Database Exception, check logs!");
                target = ProjectConstants.TARGET_LOGIN_REDIRECT;
                return target;
            }

            // Next Redirect back to login page so user can login.
            target = ProjectConstants.TARGET_LOGIN_REDIRECT;

            // Set default target.
            MessageManager.clearAllMessages();
            FacesContext ftx = FacesContext.getCurrentInstance();
            HttpServletRequest request = (HttpServletRequest) ftx.getExternalContext().getRequest();

            // Clear Registration Session
            HttpSession session = request.getSession(false);
            session.setAttribute(ProjectConstants.SESSION_AUTHORIZED, "");

            MessageManager.addInfoMessage("Registration Successful");
        }

        return target;
    }

    public Registration getRegister() {
        return register;
    }

    public void setRegister(Registration register) {
        this.register = register;
    }

    public AuthorizationManager getAuthManager() {
        return authManager;
    }

    public void setAuthManager(AuthorizationManager authManager) {
        this.authManager = authManager;
    }

    public List<SelectItem> getUserTypes() {
        return userTypes;
    }

    public void setUserTypes(List<SelectItem> userTypes) {
        this.userTypes = userTypes;
    }

    public List<SelectItem> getGenderTypes() {
        return genderTypes;
    }

    public void setGenderTypes(List<SelectItem> genderTypes) {
        this.genderTypes = genderTypes;
    }

    public String getSelectedUserType() {
        return selectedUserType;
    }

    public void setSelectedUserType(String selectedUserType) {
        this.selectedUserType = selectedUserType;
    }

    public Vendor getVendor() {
        return vendor;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

}
