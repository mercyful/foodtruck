/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.griffin.foodtruck.controller;

import com.griffin.foodtruck.constants.ProjectConstants;
import com.griffin.foodtruck.entity.Item;
import com.griffin.foodtruck.entity.Menu;
import com.griffin.foodtruck.entity.Registration;
import com.griffin.foodtruck.manager.AuthorizationManager;
import com.griffin.foodtruck.manager.MenuManager;
import com.griffin.foodtruck.manager.MessageManager;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Authentication
 *
 * @author Blue
 */
@Named
@ViewScoped
public class VendorController implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(VendorController.class.getName());
    private static final long serialVersionUID = 1L;

    List<Item> menuItems = new ArrayList<>();
    private Menu menu;
    private String newItem = "";
    private BigDecimal newPrice = new BigDecimal("0").setScale(2);

    protected AuthorizationManager authManager;
    protected MenuManager menuManager;

    /**
     * Dependency Injected Constructor
     *
     * @param authManager
     * @param menuManager
     */
    @Inject
    public VendorController(AuthorizationManager authManager, MenuManager menuManager) {
        this.authManager = authManager;
        this.menuManager = menuManager;
    }

    @PostConstruct
    public void clearMessages() {
        MessageManager.clearAllMessages();
        init();
    }

    /**
     * Startup Sequence for Populating the Data Table.
     */
public void init() {
        try {
            // We need to load the initial Menu by the existing Vendor.            
            Registration entity = authManager.retrieveRegistrationByLoginId(getUserName());
            if (entity == null) {
                LOGGER.error("Registration entity is null for user name: " + getUserName());
                menuItems = new ArrayList<>();
            }
            menu = menuManager.retrieveMenuByContact(entity);
            if (menu == null) {
                LOGGER.error("Menu entity is null for user name: " + getUserName());
                menuItems = new ArrayList<>();
            }

            try {
                if (menu.getItemCollection() != null && menu.getItemCollection().size() > 0) {
                    menuItems = new ArrayList<>(menu.getItemCollection());
                }
            } catch (NullPointerException ex) {
                // Don't worry about exception if no items have been created yet.
                menuItems = new ArrayList<>();
            }
            
            // Copy all current Values to Previous Values for Edits and Cancelations
            for(Item itm : menuItems) {
                itm.setPrevName(itm.getName());
                itm.setPrevPrice(itm.getPrice());
            }

        } catch (Exception e) {
            LOGGER.error("Exception in Vendor Controller ", e);
            MessageManager.addErrorMessage("Error retrieving Vendor Menu Information");
        }
    }

    public void refreshMenuItems() {
        try {
            LOGGER.info(" * menu.getItemCollection().size() : " + menu.getItemCollection().size());

            Registration entity = authManager.retrieveRegistrationByLoginId(getUserName());
            if (entity == null) {
                LOGGER.error("Registration entity is null for user name: " + getUserName());
                menuItems = new ArrayList<>();
            }
            menu = menuManager.retrieveMenuByContact(entity);
            if (menu == null) {
                LOGGER.error("Menu entity is null for user name: " + getUserName());
                menuItems = new ArrayList<>();
            }

            if (menu.getItemCollection() != null && menu.getItemCollection().size() > 0) {
                menuItems = new ArrayList<>(menu.getItemCollection());

            } else {
                // No Items, Clear out list and refresh it.
                menuItems = new ArrayList<>();
            }
            
            // Copy all current Values to Previous Values for Edits and Cancelations
            for(Item itm : menuItems) {
                itm.setPrevName(itm.getName());
                itm.setPrevPrice(itm.getPrice());
            }            
            
        } catch (NullPointerException ex) {
            // Don't worry about exception if no items have been created yet.
            menuItems = new ArrayList<>();
        } catch (Exception e) {
            LOGGER.error("Exception Refreshing Menu after Item Addition ", e);
        }
    }

    /**
     * Save changes to an Existing Menu Item
     *
     * @param id
     * @return
     */
    public String editItem(Integer id) {
        LOGGER.info("edit Item id: " + id);

        for (Item itm : menuItems) {
            if (itm.getIndex().equals(id)) {
                LOGGER.info(" *** Saving Updated item id: " + id);

                try {
                    menuManager.updateMenuItem(itm);
                    
                    // Reset the Edit Flag, and copy updated values to Previous value for future changes.
                    itm.setEdit(false);
                    itm.setPrevName(itm.getName());
                    itm.setPrevPrice(itm.getPrice());
                    MessageManager.addInfoGrowl("Menu Editor", "Menu Item Successfully Updated.");
                } catch (Exception ex) {
                    java.util.logging.Logger.getLogger(VendorController.class.getName()).log(Level.SEVERE, null, ex);
                }

                return null;
            }
        }

        return null;
    }

    /**
     * Cancel changes to Edit Fields, and just refresh out changes.
     * 
     * @param id
     * @return 
     */
    public String cancelItem(Integer id) {
        LOGGER.info("canceling Item edit");
        for (Item itm : menuItems) {
            if (itm.getIndex().equals(id) && itm.isEdit()) {
                LOGGER.info(" *** Canceling edits on item id: " + id);
                
                // Copy Previous Values to current Values, then remove edit flag.
                itm.setName(itm.getPrevName());
                itm.setPrice(itm.getPrevPrice());
                itm.setEdit(false);
                MessageManager.addInfoGrowl("Menu Editor", "Menu Item Edit Successfully Canceled.");
                break;
            }
        }
        
        return null;
    }

    /**
     * Delete an Existing Menu Item
     *
     * @param id
     * @return
     */
    public String deleteItem(Integer id) {
        LOGGER.info("deleting Item id: " + id);

        for (Item itm : menuItems) {
            if (itm.getIndex().equals(id)) {
                LOGGER.info(" *** Removing item id: " + id);

                try {
                    menuManager.deleteMenuItem(itm);

                    refreshMenuItems();
                    MessageManager.addInfoGrowl("Menu Editor", "Menu Item Successfully Deleted.");
                } catch (Exception ex) {
                    java.util.logging.Logger.getLogger(VendorController.class.getName()).log(Level.SEVERE, null, ex);
                }

                return null;
            }
        }

        return null;
    }

    /**
     * Add New Menu Item
     *
     * @return
     */
    public String addItem() {
        LOGGER.info("Item added to current menu" + newItem + " " + newPrice);
        Item item = new Item();
        item.setMenu(menu);
        item.setName(newItem);
        item.setPrice(newPrice);
        //menuItems.add(item);
        menuManager.saveMenuItem(item);

        LOGGER.info("Item added to current menu - completed.");
        newItem = "";
        newPrice = new BigDecimal("0").setScale(2);

        refreshMenuItems();
        MessageManager.addInfoGrowl("Menu Editor", "Menu Item Successfully Added.");
        return null;
    }

    /**
     * On Cell Edit Allows you to pull the values Change formatting or even do
     * on-the-fly saves At this time we are not doing anything special by
     * retrieving the item. for an Example.
     *
     * @param event
     * @return
     */
    public String onCellEdit(AjaxBehaviorEvent event) {
        LOGGER.info("SET EDIT ON FIELD");
        String x = event.getComponent().getClientId();
        String[] col = x.split(":");

        int index = new Integer(col[col.length - 2]);

        // update / save item ... Or change cell color to show changes were made.
        LOGGER.info("menuItems.get: " + index);
        menuItems.get(index).setEdit(true);
        return null;
    }

    /**
     * Gets User Name from the logged in Session.
     *
     * @return
     */
    public String getUserName() {
        FacesContext ftx = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) ftx.getExternalContext().getRequest();
        HttpSession session = request.getSession(false);
        String username = (String) session.getAttribute(ProjectConstants.USER_NAME);
        return username;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public Collection<Item> getItemsList() {
        return menuItems;
    }

    public void setItemsList(List<Item> itemsList) {
        this.menuItems = itemsList;
    }

    public String getNewItem() {
        return newItem;
    }

    public void setNewItem(String newItem) {
        this.newItem = newItem;
    }

    public BigDecimal getNewPrice() {
        return newPrice;
    }

    public void setNewPrice(BigDecimal newPrice) {
        this.newPrice = newPrice;
    }

}
