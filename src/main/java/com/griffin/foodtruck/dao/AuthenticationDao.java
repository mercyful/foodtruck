/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.griffin.foodtruck.dao;

import com.griffin.foodtruck.entity.Authentication;
import com.griffin.foodtruck.manager.AuthorizationManager;
import com.griffin.foodtruck.persistence.EntityManagerHelper;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Blue
 */
public class AuthenticationDao implements java.io.Serializable {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthorizationManager.class.getName());    
    protected EntityManagerHelper emh;

    public AuthenticationDao() {
        emh = EntityManagerHelper.getInstance();
    }
       
    /**
     * Find record by Login ID using JPA
     * 
     * @param loginId
     * @return
     * @throws Exception 
     */
    public Authentication findByLoginId(String loginId) throws Exception {
        
        Authentication instance = null;
        try {
            EntityManager em = emh.getEntityManager();
            emh.beginTransaction();
            String jqlString = "SELECT a "
 			+ "FROM Authentication a "
 			+ "WHERE a.loginId = :loginId ";

            // Get Instance fro JQL Query.
            TypedQuery<Authentication> query = em.createQuery(jqlString, Authentication.class);                        
            query.setParameter("loginId", loginId);
            instance = query.getSingleResult();
            emh.commit();
            LOGGER.info("Login and Password were successful - authorized: " + loginId);
        }
        catch (NoResultException nre) {            
            LOGGER.info("no authorization result found for: " + loginId);
            return null;
        }
        catch (Exception ex) {
            emh.rollback();
            throw(ex);
        }
        
        return instance;
    }
    
    /**
     * Fine record by  Login ID using a Named Query
     * Names Queries are in the Entity Class
     * 
     * @param loginId
     * @return
     * @throws Exception 
     */
    public Authentication namedQueryfindByLoginId(String loginId) throws Exception {
        
        Authentication instance = null;
        try {
            EntityManager em = emh.getEntityManager();
            emh.beginTransaction();
            
            // Get Instance fro JQL Query. Named Query
            TypedQuery<Authentication> query = em.createNamedQuery("Authentication.findByLoginId", Authentication.class);                        
            query.setParameter("loginId", loginId);
            instance = query.getSingleResult();
            emh.commit();
            LOGGER.info("Login and Password were successful - authorized: " + loginId);
        }
        catch (NoResultException nre) {            
            LOGGER.info("no authorization result found for: " + loginId);
            return null;
        }
        catch (Exception ex) {
            emh.rollback();
            throw(ex);
        }
        
        return instance;
    }
}
