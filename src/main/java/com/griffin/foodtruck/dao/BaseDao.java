/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.griffin.foodtruck.dao;

import com.griffin.foodtruck.persistence.EntityManagerHelper;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.validation.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Blue
 * @param <T>
 */
public class BaseDao<T> implements java.io.Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(BaseDao.class.getName());
    protected Class<T> entityClass;

    public BaseDao(Class<T> instanceClass) {
        entityClass = instanceClass;
    }

    public EntityManagerHelper getEntityManagerHelper() {
        return EntityManagerHelper.getInstance();
    }

    public String getInstanceName(T entity) {
        return entity.getClass().getName();
    }

    public void persist(T entity) throws Exception {
        LOGGER.info("Persisting " + getInstanceName(entity) + " instance on success");
        EntityManagerHelper emh = getEntityManagerHelper();
        try {
            persistNoCommit(entity);
            emh.commit();
            LOGGER.info(getInstanceName(entity) + "Persist successful - Transaction commited.");
        } catch (Exception ex) {
            LOGGER.error(getInstanceName(entity) + "Persist failure - Transaction rolled back.", ex);
            emh.rollback();
            throw ex;
        }
    }

    public void persistNoCommit(T entity) throws Exception {
        LOGGER.info("Persisting " + getInstanceName(entity) + " instance with no commit on success");
        EntityManagerHelper emh = getEntityManagerHelper();
        try {
            EntityManager em = emh.getEntityManager();
            emh.beginTransaction();
            em.persist(entity);

            LOGGER.info(getInstanceName(entity) + "Persist successful - Transaction not commited");
        } catch (ConstraintViolationException ex) {
            LOGGER.error(getInstanceName(entity) + "Persist failed - Transaction rolled back: ", ex);
            emh.rollback();
            throw new RuntimeException(ex);
        } catch (Exception ex) {
            LOGGER.error(getInstanceName(entity) + "Persist failed - Transaction rolled back", ex);
            emh.rollback();
            throw ex;
        }
    }
    
    public void nativeQueryUpdate(T entity, String sql) throws Exception {
        LOGGER.info("nativeQueryUpdate " + getInstanceName(entity) + " instance");
        EntityManagerHelper emh = getEntityManagerHelper();
        try {
            EntityManager em = emh.getEntityManager();
            emh.beginTransaction();
            Query qry = em.createNativeQuery(sql);
            qry.executeUpdate();            
            emh.commit();
            LOGGER.info(getInstanceName(entity) + " nativeQueryUpdate successful - Transaction commited");
        } catch (ConstraintViolationException ex) {
            LOGGER.error(getInstanceName(entity) + " nativeQueryUpdate failed - Transaction rolled back: ", ex);
            emh.rollback();
            throw new RuntimeException(ex);
        } catch (Exception ex) {
            LOGGER.error(getInstanceName(entity) + " nativeQueryUpdate failed - Transaction rolled back", ex);
            emh.rollback();
            throw ex;
        }
    }

    public void attachDirty(T entity) throws Exception {
        LOGGER.info("attaching dirty " + getInstanceName(entity) + " instance on success.");
        EntityManagerHelper emh = getEntityManagerHelper();
        try {
            attachDirtyNoCommit(entity);
            emh.commit();
            LOGGER.info(getInstanceName(entity) + " attachDirty successful - Transaction commited");
        } catch (Exception ex) {
            LOGGER.debug(getInstanceName(entity) + " attachDirty failed - Transaction rolled back", ex);
            emh.rollback();
            throw ex;
        }
    }

    public void attachDirtyNoCommit(T entity) throws Exception {
        LOGGER.info("attaching dirty " + getInstanceName(entity) + " instance with no commit on success.");
        EntityManagerHelper emh = getEntityManagerHelper();
        try {
            EntityManager em = emh.getEntityManager();

            // beginTransaction will join an existing transaction if one exists,
            // or create one if needed.
            emh.beginTransaction();
            entity = em.merge(entity);

            LOGGER.info(getInstanceName(entity) + " attachDirty successful - Transaction not commited");
        } catch (Exception ex) {
            LOGGER.error(getInstanceName(entity) + " attachDirty failed - Transaction rolled back", ex);
            emh.rollback();
            throw ex;
        }
    }

    public void delete(T entity) throws Exception {
        LOGGER.info("deleting " + getInstanceName(entity) + " instance");
        EntityManagerHelper emh = getEntityManagerHelper();
        try {
            EntityManager em = emh.getEntityManager();

            emh.beginTransaction();
            T instance = em.merge(entity);
            em.remove(instance);
            emh.commit();

            LOGGER.info(getInstanceName(entity) + " delete successful");
        } catch (Exception ex) {
            LOGGER.error(getInstanceName(entity) + " delete failed", ex);
            emh.rollback();
            throw ex;
        }
    }

    public T merge(T entity) throws Exception {
        LOGGER.info("merging " + getInstanceName(entity) + " instance");
        EntityManagerHelper emh = getEntityManagerHelper();
        try {
            EntityManager em = emh.getEntityManager();
            emh.beginTransaction();
            entity = em.merge(entity);
            emh.commit();

            LOGGER.info(getInstanceName(entity) + " merge successful");
        } catch (Exception ex) {
            LOGGER.error(getInstanceName(entity) + " merge failed", ex);
            emh.rollback();
            throw ex;
        }

        return entity;
    }

    public T findById(int primaryKey) {
        LOGGER.info("retriving " + entityClass + " by primaryKey: " + primaryKey);
        T entity = null;
        EntityManagerHelper emh = getEntityManagerHelper();
        try {
            EntityManager em = emh.getEntityManager();
            entity = em.find(entityClass, primaryKey);
            if (entity == null) {
                LOGGER.debug(entityClass + " findById() successful, no instance found");
            } else {
                LOGGER.debug(entityClass + " findById() successful, instance found");
            }
        } catch (NoResultException ex) {
            LOGGER.info(entityClass + " findById - no instance found. " + ex.getMessage());
        } catch (Exception ex) {
            LOGGER.error(entityClass + " findById failed: ", ex);
            throw ex;
        }

        return entity;
    }

    public T detach(T entity) throws Exception {
        LOGGER.info("getting (detached dirty) " + entityClass + " instance");
        EntityManagerHelper emh = getEntityManagerHelper();
        try {
            EntityManager em = emh.getEntityManager();

            emh.beginTransaction();
            em.detach(entity);
            emh.commit();
            LOGGER.info(getInstanceName(entity) + " detach successful");
        } catch (IllegalArgumentException ex) {
            LOGGER.error(getInstanceName(entity) + "detach failed: ", ex);
            emh.rollback();
            throw ex;
        } catch (Exception ex) {
            LOGGER.error(getInstanceName(entity) + "detach failed: ", ex);
            emh.rollback();
            throw ex;
        }

        return entity;
    }
    
    public List<T> findAll() {
        LOGGER.info("retriving all " + entityClass + " instances");
        List<T> entities = null;
        EntityManagerHelper emh = EntityManagerHelper.getInstance();
        try {
            EntityManager em = emh.getEntityManager();
            CriteriaQuery<T> cq = em.getCriteriaBuilder().createQuery(entityClass);
            cq.select(cq.from(entityClass));
            entities = em.createQuery(cq).getResultList();
            LOGGER.info(entityClass + " retrieve all successful");
        } catch (NoResultException ex) {
            LOGGER.info(entityClass + " findAll - no instance found. " + ex.getMessage());
        } catch (Exception ex) {
            LOGGER.error(entityClass + " getting all failed", ex);
        }

        return entities;
    }
}
