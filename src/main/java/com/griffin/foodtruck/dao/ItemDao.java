/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.griffin.foodtruck.dao;

import com.griffin.foodtruck.entity.Item;
import com.griffin.foodtruck.persistence.EntityManagerHelper;
import javax.persistence.EntityManager;
import javax.persistence.FlushModeType;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Blue
 */
public class ItemDao implements java.io.Serializable {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(ItemDao.class.getName());    
    protected EntityManagerHelper emh;

    public ItemDao() {
        emh = EntityManagerHelper.getInstance();
    }
    
    /**
     * Remove a Record from the Item Table.
     * Sometimes when dealing with Entity Managers and other 
     * Session items - when you need to delete a record
     * you have to use a Native Query which bypasses JPA and just runs the 
     * SQL in the Server.
     * 
     * @param entity
     * @throws Exception 
     */
    public void delete(Item entity) throws Exception {
        
        try {
            EntityManager em = emh.getEntityManager();
            emh.beginTransaction();
                        
            // MYSQL IS stupid is as S#$T, and needs `  ` around the field names to execute properly.
            // difference from JPA Queries that don't need sepcific syntex, but if you get errors
            // then try working with Native Queries.
            Query qry = em.createNativeQuery("delete from item where `index` = :index ");
            qry.setParameter("index", entity.getIndex());
            int result = qry.executeUpdate();
            
            emh.commit();
            LOGGER.info("Item Removed: " + result);
        }
        catch (NoResultException nre) {            
            LOGGER.info("No Item result found for: " + entity.getClass().getName());
        }
        catch (Exception ex) {
            LOGGER.error("Entity Manager Exception:", ex);
            throw(ex);
        }
    }
    
    /**
     * Remove a Record from the Item Table.
     * Sometimes when dealing with Entity Managers and other 
     * Session items - when you need to delete a record
     * you have to use a Native Query which bypasses JPA and just runs the 
     * SQL in the Server.
     * 
     * @param entity
     * @throws Exception 
     */
    public void update(Item entity) throws Exception {
        
        try {
            EntityManager em = emh.getEntityManager();
            emh.beginTransaction();
                        
            // MYSQL IS stupid is as S#$T, and needs `  ` around the field names to execute properly.
            // difference from JPA Queries that don't need sepcific syntex, but if you get errors
            // then try working with Native Queries.
            Query qry = em.createNativeQuery("update item set `name` = :name, `price` = :price where `index` = :index ");
            qry.setParameter("index", entity.getIndex());
            qry.setParameter("name", entity.getName());
            qry.setParameter("price", entity.getPrice());
            
            int result = qry.executeUpdate();
            
            emh.commit();
            LOGGER.info("Item Updated: " + result);
        }
        catch (NoResultException nre) {            
            LOGGER.info("No Item result found for: " + entity.getClass().getName());
        }
        catch (Exception ex) {
            LOGGER.error("Entity Manager Exception:", ex);
            throw(ex);
        }
    }
}
