/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.griffin.foodtruck.dao;

import com.griffin.foodtruck.entity.Menu;
import com.griffin.foodtruck.entity.Vendor;
import com.griffin.foodtruck.persistence.EntityManagerHelper;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Blue
 */
public class MenuDao implements java.io.Serializable {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(MenuDao.class.getName());    
    protected EntityManagerHelper emh;

    public MenuDao() {
        emh = EntityManagerHelper.getInstance();
    }
    
    /**
     * Fine record by  Login ID using a Named Query
     * Names Queries are in the Entity Class
     * 
     * @param entity
     * @return
     * @throws Exception 
     */
    public Menu namedQueryFindByVendor(Vendor entity) throws Exception {
        
        Menu instance = null;
        try {
            EntityManager em = emh.getEntityManager();
            emh.beginTransaction();
            
            // Get Instance fro JQL Query. Named Query
            TypedQuery<Menu> query = em.createNamedQuery("Menu.findByVendor", Menu.class);                        
            query.setParameter("vendor", entity);
            instance = query.getSingleResult();
            emh.commit();
            LOGGER.info("Menu Found: " + entity.getName());
        }
        catch (NoResultException nre) {            
            LOGGER.error("no menu found by vendor: " + entity.getName(), nre);
            return null;
        }
        catch (Exception ex) {
            emh.rollback();
            throw(ex);
        }
        
        return instance;
    }
}
