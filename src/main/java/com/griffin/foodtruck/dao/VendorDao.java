/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.griffin.foodtruck.dao;

import com.griffin.foodtruck.entity.Registration;
import com.griffin.foodtruck.entity.Vendor;
import com.griffin.foodtruck.persistence.EntityManagerHelper;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Blue
 */
public class VendorDao implements java.io.Serializable {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(VendorDao.class.getName());    
    protected EntityManagerHelper emh;

    public VendorDao() {
        emh = EntityManagerHelper.getInstance();
    }
    
    /**
     * Fine record by Contact using a Named Query
     * Names Queries are in the Entity Class
     * 
     * @param entity
     * @return
     * @throws Exception 
     */
    public Vendor namedQueryfindByContact(Registration entity) throws Exception {
        
        Vendor instance = null;
        try {
            EntityManager em = emh.getEntityManager();
            emh.beginTransaction();
            
            // Get Instance fro JQL Query. Named Query
            TypedQuery<Vendor> query = em.createNamedQuery("Vendor.findByContact", Vendor.class);                        
            query.setParameter("contact", entity);
            instance = query.getSingleResult();
            emh.commit();
            LOGGER.info("Vendor Found: " + instance.getName());
        }
        catch (NoResultException nre) {            
            LOGGER.info("No Vendor result found for: " + entity.getClass().getName());
            return null;
        }
        catch (Exception ex) {
            emh.rollback();
            throw(ex);
        }
        
        return instance;
    }
}
