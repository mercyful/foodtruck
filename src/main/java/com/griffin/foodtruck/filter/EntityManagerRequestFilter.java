/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.griffin.foodtruck.filter;

import com.griffin.foodtruck.persistence.EntityManagerHelper;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Entity Manager Filter closes call sessions 
 * after each request is completed
 * 
 * This replaces the need to manually add em.close() at the end of each call
 * And also handles closing the ThreadLocal instance properly too.
 * 
 * @author Blue
 */
public class EntityManagerRequestFilter implements Filter {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(EntityManagerRequestFilter.class.getName());
    public EntityManagerRequestFilter() {}    
    
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) 
            throws IOException, ServletException {
        
        try {
            EntityManagerHelper.getInstance();                    
        }
        catch (Exception ex) {
            // Remove Stack Track, just get general message, can dig in later if needed.
            LOGGER.error("EntityManagerRequestFilter getInstance: " + ex.getMessage());
        }
        
        try {
            chain.doFilter(request, response);        
        } 
        catch (IOException | ServletException e) {
            LOGGER.error("EntityManagerRequestFilter doFilter: " + e.getMessage());
        }
        
        try {
            EntityManagerHelper.getInstance().closeEntityManager();
        } catch (Exception ex) {
            // Remove Stack Track, just get general message, can dig in later if needed.
            LOGGER.error("EntityManagerRequestFilter closeEntityManager: " + ex.getMessage());
        }
    }

    @Override
    public void init(FilterConfig arg0) throws ServletException {}

    @Override
    public void destroy() {}
    
}
