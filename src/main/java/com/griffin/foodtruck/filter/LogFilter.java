/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.griffin.foodtruck.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Log Filter pull Remote IP addresses for each POST/GET requests that comes
 * into the Server
 * 
 * @author Blue
 */
public class LogFilter implements Filter {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(LogFilter.class.getName());
  
    /**
     * Startup method for this filter
     * @param filterConfig
     * @throws javax.servlet.ServletException
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {}
    
    /**
     *
     * @param request The servlet request we are processing
     * @param response The servlet response we are creating
     * @param chain The filter chain we are processing
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet error occurs
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        
        try {
            // IP address of the client machine.   
            String remoteAddress = request.getRemoteAddr();
 
            // Returns the remote address
            LOGGER.info("Remote Address: " + remoteAddress);
             
        } catch (Exception ex) {
            LOGGER.error("Exception LogFilter getting remote address", ex);
        }
        finally {
            // Always chain next filter in a finally block
            
            try {
                chain.doFilter(request, response);
            }
            catch (IOException | ServletException e) {
                // Just post the message for now, We can use a trace if needed.
                // Less Stack Trackes for warning etc.. the better.
                LOGGER.error("Uncaught Exception: " + e.getMessage());
            }
        }                         
    }

    /**
     * Destroy method for this filter
     */
    @Override
    public void destroy() {}

}
