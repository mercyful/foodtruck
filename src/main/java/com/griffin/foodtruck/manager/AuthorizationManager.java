/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.griffin.foodtruck.manager;

import com.griffin.foodtruck.dao.AuthenticationDao;
import com.griffin.foodtruck.dao.BaseDao;
import com.griffin.foodtruck.entity.Authentication;
import com.griffin.foodtruck.entity.Registration;
import com.griffin.foodtruck.entity.UserTypeLk;
import com.griffin.foodtruck.entity.Vendor;
import java.io.Serializable;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Blue
 */
public class AuthorizationManager implements Serializable {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthorizationManager.class.getName());    
    protected AuthenticationDao authDao;
    protected BaseDao<UserTypeLk> baseDaoUserType;
    protected BaseDao<Vendor> baseDaoVendor;
    protected BaseDao<Authentication> baseDaoAuthentication;
    
    /**
     * Secondary Constructor, USed for Unit Testing
     * @param authDao
     * @param baseDaoUserType
     * @param baseDaoVendor
     * @param baseDaoAuthentication 
     */
    protected AuthorizationManager(AuthenticationDao authDao, BaseDao<UserTypeLk> baseDaoUserType, 
            BaseDao<Vendor> baseDaoVendor, BaseDao<Authentication> baseDaoAuthentication) {
        this.authDao = authDao;
        this.baseDaoUserType = baseDaoUserType;
        this.baseDaoVendor = baseDaoVendor;
        this.baseDaoAuthentication = baseDaoAuthentication;
    }

    /**
     * Primary Constructor
     * Calls into Secondary Constructor
     */
    public AuthorizationManager() {
        this(new AuthenticationDao(), new BaseDao<>(UserTypeLk.class), new BaseDao<>(Vendor.class), new BaseDao<>(Authentication.class));
    }

    /**
     * Check Login Authorization
     * 
     * @param loginId
     * @param password
     * @return
     * @throws Exception 
     */
    public boolean checkAuthorization(String loginId, String password) throws Exception {
        
        // Call the Data Access Object - Database Lookup.
        Authentication instance = authDao.namedQueryfindByLoginId(loginId);
        
        // Check if Password Matches
        if (instance == null || !instance.getPassword().equals(password)) {
            LOGGER.info("Invalid Password for: " + loginId);
            return false;
        }
        
        LOGGER.info("Login and Password were successful - authorized: " + loginId);
        return true;
    }
    
    /**
     * Check Register Authorization - 
     * if Previous Authorization Exists Prior to creating a new one, is this set valid.
     * 
     * @param loginId
     * @return
     * @throws Exception 
     */
    public boolean doesAuthorizationExist(String loginId) throws Exception {
        
        // Call the Data Access Object - Database Lookup.
        Authentication instance = authDao.findByLoginId(loginId);
        
        // Check if Record with UserName already exists.
        if (instance == null) {
            LOGGER.info("New Authorization is valid for: " + loginId);
            return false;
        }
        
        LOGGER.info("Authorization already exists for: " + loginId);
        return true;
    }
    
    /**
     * Check Register Authorization - 
     * if Previous Authorization Exists Prior to creating a new one, is this set valid.
     * 
     * @param loginId
     * @return
     * @throws Exception 
     */
    public Registration retrieveRegistrationByLoginId(String loginId) throws Exception {
        
        // Call the Data Access Object - Database Lookup.
        Authentication instance = authDao.findByLoginId(loginId);
        
        // Check if Record with UserName already exists.
        if (instance == null) {
            LOGGER.info("Registration not found for: " + loginId);
            return null;
        }
        
        LOGGER.info("Registration retrieved for: " + loginId);
        return instance.getRegistration();
    }
    
    /**
     * Retrieve a List of All User Types
     * 
     * @return 
     */
    public List<UserTypeLk> getUserTypes() {
        List<UserTypeLk> userTypes = baseDaoUserType.findAll();
        return userTypes;
    }
    
    /**
     * Save Registration with Authorization
     * 
     * @param entity 
     */
    public void saveRegistration(Authentication entity) {
        try {
            baseDaoAuthentication.persist(entity);
            
        } catch (Exception ex) {
            LOGGER.error("Save Registration Persist Error: ", ex);
        }
    }
    
    /**
     * Save a new Vendor Record
     * 
     * @param entity 
     * @throws java.lang.Exception 
     */
    public void saveVendor(Vendor entity) throws Exception {
        try {
            baseDaoVendor.persist(entity);
            
        } catch (Exception ex) {
            LOGGER.error("Save Vendor Persist Error: ", ex);
            throw ex;
        }
    }
}
