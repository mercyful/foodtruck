/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.griffin.foodtruck.manager;

import com.griffin.foodtruck.dao.BaseDao;
import com.griffin.foodtruck.dao.ItemDao;
import com.griffin.foodtruck.dao.MenuDao;
import com.griffin.foodtruck.dao.VendorDao;
import com.griffin.foodtruck.entity.Item;
import com.griffin.foodtruck.entity.Menu;
import com.griffin.foodtruck.entity.Registration;
import com.griffin.foodtruck.entity.Vendor;
import java.io.Serializable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Blue
 */
public class MenuManager implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(MenuManager.class.getName());
    protected MenuDao menuDao;
    protected VendorDao vendorDao;
    protected ItemDao itemDao;
    protected BaseDao<Menu> baseDaoMenu;
    protected BaseDao<Item> baseDaoItem;

    /**
     * Secondary Constructor, USed for Unit Testing
     *
     * @param baseDaoMenu
     * @param vendorDao
     * @param itemDao
     * @param menuDao
     * @param baseDaoItem
     */
    protected MenuManager(BaseDao<Menu> baseDaoMenu, VendorDao vendorDao, ItemDao itemDao, 
            MenuDao menuDao, BaseDao<Item> baseDaoItem) {
        this.baseDaoMenu = baseDaoMenu;
        this.vendorDao = vendorDao;
        this.itemDao = itemDao;
        this.menuDao = menuDao;
        this.baseDaoItem = baseDaoItem;
    }

    /**
     * Primary Constructor Calls into Secondary Constructor
     */
    public MenuManager() {
        this(new BaseDao<>(Menu.class), new VendorDao(), new ItemDao(), new MenuDao(), new BaseDao<>(Item.class));
    }

    /**
     * Retrieve a List of All User Types
     *
     * @param entity
     */
    public void saveMenu(Menu entity) throws Exception {
        try {
            baseDaoMenu.persist(entity);

        } catch (Exception ex) {
            LOGGER.error("Save Menu Error: ", ex);
            throw ex;
        }
    }
    
    /**
     * Retrieve a Menu By Vendor Entity
     *
     * @param entity
     * @throws java.lang.Exception
     */
    public void updateMenu(Menu entity) throws Exception {
        try {
            baseDaoMenu.merge(entity);

        } catch (Exception ex) {
            LOGGER.error("Save Menu Error: ", ex);
        }
    }

    /**
     * Retrieve a Menu By Vendor Entity
     *
     * @param entity
     * @return
     * @throws java.lang.Exception
     */
    public Menu retrieveMenu(Vendor entity) throws Exception {
        return menuDao.namedQueryFindByVendor(entity);
    }

    /**
     * Retrieve Menu by Registration entity.
     *
     * @param entity
     * @return
     * @throws java.lang.Exception
     */
    public Menu retrieveMenuByContact(Registration entity) throws Exception {
        Vendor vendor = vendorDao.namedQueryfindByContact(entity);
        return retrieveMenu(vendor);
    }

    /**
     * Save a Menu Item (Using Base DAO Persist)
     *
     * @param entity
     */
    public void saveMenuItem(Item entity) {
        try {
            baseDaoItem.persist(entity);

        } catch (Exception ex) {
            LOGGER.error("Save Menu Item Error: ", ex);
        }
    }

    /**
     * Delete a Menu Item using
     * Native SQL that is run on the server. This will run a delete.
     *
     * BaseDAO remove seems to be having issues with MySQL Dialect 
     * 
     * @param entity
     */
    public void deleteMenuItem(Item entity) {
        try {
            itemDao.delete(entity);

        } catch (Exception ex) {
            LOGGER.error("Delete Menu Item Error: ", ex);
        }
    }
    
    /**
     * Update a Menu Item using
     * Native SQL that is run on the server. This will run an update.
     *
     * BaseDAO merge seems to be having issues with MySQL Dialect 
     * 
     * @param entity
     */
    public void updateMenuItem(Item entity) {
        try {
            itemDao.update(entity);

        } catch (Exception ex) {
            LOGGER.error("Update Menu Item Error: ", ex);
        }
    }
}
