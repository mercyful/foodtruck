/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.griffin.foodtruck.manager;

import java.util.Iterator;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;

/**
 *
 * @author Blue
 */
public class MessageManager {

    public static void addInfoGrowl(String component, String message) {
        addMessage(null, createMessageGrowl(component, message, FacesMessage.SEVERITY_INFO));
    }
    
    public static void addInfoMessage(String message) {
        addInfoMessage(null, message);
    }

    public static void addInfoMessage(String jsfComponentId, String message) {
        addMessage(jsfComponentId, createMessage(message, FacesMessage.SEVERITY_INFO));
    }

    public static void addWarningMessage(String message) {
        addWarningMessage(null, message);
    }

    public static void addWarningMessage(String jsfComponentId, String message) {
        addMessage(jsfComponentId, createMessage(message, FacesMessage.SEVERITY_WARN));
    }

    public static void addErrorMessage(String message) {
        addErrorMessage(null, message);
    }

    public static void addErrorMessage(String jsfComponentId, String message) {
        addMessage(jsfComponentId, createMessage(message, FacesMessage.SEVERITY_ERROR));
    }

    public static void addFatalMessage(String message) {
        addErrorMessage(null, message);
    }

    public static void addFatalMessage(String jsfComponentId, String message) {
        addMessage(jsfComponentId, createMessage(message, FacesMessage.SEVERITY_FATAL));
    }

    public static void clearAllMessages() {
        FacesContext context = FacesContext.getCurrentInstance();
        Iterator<FacesMessage> it = context.getMessages();
        while (it.hasNext()) {
            it.next();
            it.remove();
        }
    }

    private static FacesMessage createMessage(String message, Severity severity) {
        return new FacesMessage(severity, null, message);
    }
    
    private static FacesMessage createMessageGrowl(String component, String message, Severity severity) {
        return new FacesMessage(severity, component, message);
    }

    private static void addMessage(String jsfComponentId, FacesMessage facesMessage) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        facesContext.getExternalContext().getFlash().setKeepMessages(true);
        facesContext.addMessage(jsfComponentId, facesMessage);
    }
}
