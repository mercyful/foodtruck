/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.griffin.foodtruck.persistence;

import java.io.Serializable;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Blue
 */
public class EntityManagerHelper implements Serializable {
    private static final Logger LOGGER = LoggerFactory.getLogger(EntityManagerHelper.class.getName());
    
    protected static volatile EntityManagerHelper entityManagerHelper;
    protected static volatile EntityManagerFactory emf;
    protected static final ThreadLocal<EntityManager> ENTITY_MANAGER = new ThreadLocal<>();
    
    private EntityManagerHelper() {}
    
    /**
     * Thread-Safe Singleton Instance
     * @return 
     */
    public static EntityManagerHelper getInstance() {
        if (entityManagerHelper == null) {
            synchronized(EntityManagerHelper.class) {
                try {
                    if (entityManagerHelper == null) {
                        entityManagerHelper = new EntityManagerHelper();
                        
                        // This uses a Persistence Unit for foodTruckDS DataSource 
                        emf = Persistence.createEntityManagerFactory("foodTruckPU");
                    }
                } catch (Exception ex) {
                    LOGGER.error("Initial EntityManagerFactory creation failed in getInstance(). ", ex);
                    throw new ExceptionInInitializerError(ex);
                }
            }
        }
        return entityManagerHelper;
    }
    
    /**
     * Get Entity Manager From Thread Local
     * Per session entity manager
     * @return 
     */
    public EntityManager getEntityManager() {
        EntityManager em = ENTITY_MANAGER.get();
        if (em == null) {
            em = emf.createEntityManager();
            ENTITY_MANAGER.set(em);
        }

        return em;
    }
    
    /**
     * Close EntityManager for existing session
     */
    public void closeEntityManager() {
        EntityManager em = ENTITY_MANAGER.get();
        if ((em != null) && (em.isOpen())) {
            em.close();
            ENTITY_MANAGER.set(null);
        }
    }
    
    /**
     * Close EntityManagerFactory
     */
    public void closeEntityManagerFactory() {
        closeEntityManager();
        if ((emf != null) && (emf.isOpen())) {
                emf.close();
        }
    }
    
    /**
     * Begin Transaction 
     * @throws Exception 
     */
    public void beginTransaction() throws Exception {
        try {
            EntityTransaction txn = getEntityManager().getTransaction();
	    if ((txn != null) && (!txn.isActive())) {
	    	getEntityManager().getTransaction().begin();
	    }
    	} catch (Exception ex) {
            LOGGER.error("Error creating transaction: ", ex);
            throw ex;
    	}
    }

    /**
     * Commit Transaction
     * @throws Exception 
     */
    public void commit() throws Exception {
        try {
            EntityTransaction txn = getEntityManager().getTransaction();
	    if ((txn != null) && (txn.isActive())) {
	    	getEntityManager().getTransaction().commit();
	    }
    	} catch (Exception ex) {
            LOGGER.error("Error committing transaction: ", ex);
            throw ex;
    	}
    }

    /**
     * Rollback Transaction on Error or Issues
     */
    public void rollback() {
	EntityManager em = getEntityManager();
	try {
            EntityTransaction txn = em.getTransaction();
            if (em.getTransaction() == null) {
                LOGGER.warn("EntityManagerHelper.rollback() called on null transaction.");
                return;
            } else if (!em.getTransaction().isActive()) {
                LOGGER.warn("EntityMangerHelper.rollback(); called with non-active transaction.");
                return;
            }
            
            txn.rollback();
        } catch (Exception ex) {
            LOGGER.error("Exception rolling back a transaction: ", ex);
            throw ex;
        }
    }
}
